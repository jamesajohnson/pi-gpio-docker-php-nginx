<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="A portfolio template that uses Material Design Lite.">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
  <title>Pi Relay</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue_grey-red.min.css" />
  <link rel="stylesheet" href="css/styles.css" />


  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>

  <!-- are we toggling a device -->
  <?php
  if( isset( $_GET["pin"] ) && isset( $_GET["switch"] ) ) {
    $pin = $_GET["pin"];
    $switch = $_GET["switch"];
    $switch = $switch == "off" ? 1 : 0;

    $cmd_setMode = 'gpio mode ' . $pin . ' out';
    $cmd_toggle = 'gpio write ' . $pin . ' ' . $switch;

    echo exec($cmd_setMode);
    echo exec($cmd_toggle);
  }
  ?>

  <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <header class="mdl-layout__header">
      <div class="mdl-layout-icon"></div>
      <div class="mdl-layout__header-row">
        <span class="mdl-layout__title">Simple Layout</span>
        <div class="mdl-layout-spacer"></div>
        <nav class="mdl-navigation">
          <!--<a class="mdl-navigation__link" href="#">Nav link 1</a>-->
        </nav>
      </div>
    </header>
    <!--<div class="mdl-layout__drawer">
      <span class="mdl-layout__title">Simple Layout</span>
      <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="#">Nav link 1</a>
        <a class="mdl-navigation__link" href="#">Nav link 2</a>
        <a class="mdl-navigation__link" href="#">Nav link 3</a>
      </nav>
    </div>-->
    <main id="app" class="mdl-layout__content">
      <div class="page-content">
        <textarea :value="input" @input="update"></textarea>
        <div class="mdl-grid">
          <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--12-col-phone">
            <!-- room -->
            <div class="mdl-card mdl-shadow--2dp">
              <div class="mdl-card__title mdl-card--expand">
                <h2 class="mdl-card__title-text">Update</h2>
              </div>
              <div class="mdl-card__supporting-text">
                <!-- item -->
                <form method="get">
                  <input type="hidden" name="pin" value="4">
                  <div class="mdl-list__item">
                      <span class="mdl-list__item-primary-content">
                        Light 1
                      </span>
                      <span class="mdl-list__item-secondary-action">
                        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="list-switch-1">
                          <input type="checkbox" name="switch" class="mdl-switch__input"  onChange="this.form.submit()"/>
                        </label>
                      </span>
                    </div>
                </form>
              </div>
              <div class="mdl-card__actions mdl-card--border">
                <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                  View Updates
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>
  <!-- Vuejs 2 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.common.min.js"></script>
</body>
</html>
